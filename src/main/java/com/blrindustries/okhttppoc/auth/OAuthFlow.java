package com.blrindustries.okhttppoc.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}
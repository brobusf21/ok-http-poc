package com.blrindustries.okhttppoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VehicleIdsResponse {
    @SerializedName("vehicleIds")
    private List<Integer> vehicleIds = null;

    public VehicleIdsResponse vehicleIds(List<Integer> vehicleIds) {
        this.vehicleIds = vehicleIds;
        return this;
    }

    public VehicleIdsResponse addVehicleIdsItem(Integer vehicleIdsItem) {
        if (this.vehicleIds == null) {
            this.vehicleIds = new ArrayList<Integer>();
        }
        this.vehicleIds.add(vehicleIdsItem);
        return this;
    }

    /**
     * Get vehicleIds
     * @return vehicleIds
     **/
    public List<Integer> getVehicleIds() {
        return vehicleIds;
    }

    public void setVehicleIds(List<Integer> vehicleIds) {
        this.vehicleIds = vehicleIds;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VehicleIdsResponse vehicleIdsResponse = (VehicleIdsResponse) o;
        return Objects.equals(this.vehicleIds, vehicleIdsResponse.vehicleIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vehicleIds);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class VehicleIdsResponse {\n");

        sb.append("    vehicleIds: ").append(toIndentedString(vehicleIds)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

package com.blrindustries.okhttppoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Answer {
    @SerializedName("dealers")
    private List<DealerAnswer> dealers = null;

    public Answer dealers(List<DealerAnswer> dealers) {
        this.dealers = dealers;
        return this;
    }

    public Answer addDealersItem(DealerAnswer dealersItem) {
        if (this.dealers == null) {
            this.dealers = new ArrayList<DealerAnswer>();
        }
        this.dealers.add(dealersItem);
        return this;
    }

    /**
     * Get dealers
     * @return dealers
     **/
    public List<DealerAnswer> getDealers() {
        return dealers;
    }

    public void setDealers(List<DealerAnswer> dealers) {
        this.dealers = dealers;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Answer answer = (Answer) o;
        return Objects.equals(this.dealers, answer.dealers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dealers);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Answer {\n");

        sb.append("    dealers: ").append(toIndentedString(dealers)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

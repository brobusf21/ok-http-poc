package com.blrindustries.okhttppoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class DealersResponse {
    @SerializedName("dealerId")
    private Integer dealerId = null;

    @SerializedName("name")
    private String name = null;

    public DealersResponse dealerId(Integer dealerId) {
        this.dealerId = dealerId;
        return this;
    }

    /**
     * Get dealerId
     * @return dealerId
     **/
    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public DealersResponse name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * @return name
     **/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DealersResponse dealersResponse = (DealersResponse) o;
        return Objects.equals(this.dealerId, dealersResponse.dealerId) &&
                Objects.equals(this.name, dealersResponse.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dealerId, name);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DealersResponse {\n");

        sb.append("    dealerId: ").append(toIndentedString(dealerId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

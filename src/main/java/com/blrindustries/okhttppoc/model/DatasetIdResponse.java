package com.blrindustries.okhttppoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class DatasetIdResponse {
    @SerializedName("datasetId")
    private String datasetId = null;

    public DatasetIdResponse datasetId(String datasetId) {
        this.datasetId = datasetId;
        return this;
    }

    /**
     * Get datasetId
     * @return datasetId
     **/
    public String getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(String datasetId) {
        this.datasetId = datasetId;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DatasetIdResponse datasetIdResponse = (DatasetIdResponse) o;
        return Objects.equals(this.datasetId, datasetIdResponse.datasetId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datasetId);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DatasetIdResponse {\n");

        sb.append("    datasetId: ").append(toIndentedString(datasetId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

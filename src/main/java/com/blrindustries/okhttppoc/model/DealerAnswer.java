package com.blrindustries.okhttppoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DealerAnswer {
    @SerializedName("dealerId")
    private Integer dealerId = null;

    @SerializedName("name")
    private String name = null;

    @SerializedName("vehicles")
    private List<VehicleAnswer> vehicles = null;

    public DealerAnswer dealerId(Integer dealerId) {
        this.dealerId = dealerId;
        return this;
    }

    /**
     * Get dealerId
     * @return dealerId
     **/
    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public DealerAnswer name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     * @return name
     **/
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DealerAnswer vehicles(List<VehicleAnswer> vehicles) {
        this.vehicles = vehicles;
        return this;
    }

    public DealerAnswer addVehiclesItem(VehicleAnswer vehiclesItem) {
        if (this.vehicles == null) {
            this.vehicles = new ArrayList<VehicleAnswer>();
        }
        this.vehicles.add(vehiclesItem);
        return this;
    }

    /**
     * Get vehicles
     * @return vehicles
     **/
    public List<VehicleAnswer> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleAnswer> vehicles) {
        this.vehicles = vehicles;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DealerAnswer dealerAnswer = (DealerAnswer) o;
        return Objects.equals(this.dealerId, dealerAnswer.dealerId) &&
                Objects.equals(this.name, dealerAnswer.name) &&
                Objects.equals(this.vehicles, dealerAnswer.vehicles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dealerId, name, vehicles);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DealerAnswer {\n");

        sb.append("    dealerId: ").append(toIndentedString(dealerId)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    vehicles: ").append(toIndentedString(vehicles)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

package com.blrindustries.okhttppoc.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class AnswerResponse {
    @SerializedName("success")
    private Boolean success = null;

    @SerializedName("message")
    private String message = null;

    @SerializedName("totalMilliseconds")
    private Integer totalMilliseconds = null;

    public AnswerResponse success(Boolean success) {
        this.success = success;
        return this;
    }

    /**
     * Get success
     * @return success
     **/
    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public AnswerResponse message(String message) {
        this.message = message;
        return this;
    }

    /**
     * Get message
     * @return message
     **/
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AnswerResponse totalMilliseconds(Integer totalMilliseconds) {
        this.totalMilliseconds = totalMilliseconds;
        return this;
    }

    /**
     * Get totalMilliseconds
     * @return totalMilliseconds
     **/
    public Integer getTotalMilliseconds() {
        return totalMilliseconds;
    }

    public void setTotalMilliseconds(Integer totalMilliseconds) {
        this.totalMilliseconds = totalMilliseconds;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnswerResponse answerResponse = (AnswerResponse) o;
        return Objects.equals(this.success, answerResponse.success) &&
                Objects.equals(this.message, answerResponse.message) &&
                Objects.equals(this.totalMilliseconds, answerResponse.totalMilliseconds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(success, message, totalMilliseconds);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AnswerResponse {\n");

        sb.append("    success: ").append(toIndentedString(success)).append("\n");
        sb.append("    message: ").append(toIndentedString(message)).append("\n");
        sb.append("    totalMilliseconds: ").append(toIndentedString(totalMilliseconds)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
package com.blrindustries.okhttppoc.api;

import com.blrindustries.okhttppoc.configuration.AppConfig;
import com.blrindustries.okhttppoc.exception.ApiException;
import com.blrindustries.okhttppoc.helper.*;
import com.blrindustries.okhttppoc.model.DealersResponse;
import com.google.gson.reflect.TypeToken;
import com.blrindustries.okhttppoc.helper.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Component
public class DealersApi {
    private ApiClient apiClient;

    public DealersApi() {
        this(AppConfig.getDefaultApiClient());
    }

    public DealersApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for dealersGetDealer
     * @param datasetId  (required)
     * @param dealerid  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call dealersGetDealerCall(String datasetId, Integer dealerid, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/{datasetId}/dealers/{dealerid}"
                .replaceAll("\\{" + "datasetId" + "\\}", apiClient.escapeString(datasetId.toString()))
                .replaceAll("\\{" + "dealerid" + "\\}", apiClient.escapeString(dealerid.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json", "text/json", "text/html", "application/xml", "text/xml"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {

        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call dealersGetDealerValidateBeforeCall(String datasetId, Integer dealerid, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        // verify the required parameter 'datasetId' is set
        if (datasetId == null) {
            throw new ApiException("Missing the required parameter 'datasetId' when calling dealersGetDealer(Async)");
        }

        // verify the required parameter 'dealerid' is set
        if (dealerid == null) {
            throw new ApiException("Missing the required parameter 'dealerid' when calling dealersGetDealer(Async)");
        }


        com.squareup.okhttp.Call call = dealersGetDealerCall(datasetId, dealerid, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get information about a dealer
     *
     * @param datasetId  (required)
     * @param dealerid  (required)
     * @return DealersResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public DealersResponse dealersGetDealer(String datasetId, Integer dealerid) throws ApiException {
        ApiResponse<DealersResponse> resp = dealersGetDealerWithHttpInfo(datasetId, dealerid);
        return resp.getData();
    }

    /**
     * Get information about a dealer
     *
     * @param datasetId  (required)
     * @param dealerid  (required)
     * @return ApiResponse&lt;DealersResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<DealersResponse> dealersGetDealerWithHttpInfo(String datasetId, Integer dealerid) throws ApiException {
        com.squareup.okhttp.Call call = dealersGetDealerValidateBeforeCall(datasetId, dealerid, null, null);
        Type localVarReturnType = new TypeToken<DealersResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get's information about a dealer asynchronously
     * @param datasetId    (required)
     * @param dealerId    (required)
     * @return The dealer response
     */
    public CompletableFuture<DealersResponse> getDealerAsync(String datasetId, Integer dealerId){

        CompletableFuture<DealersResponse> future = CompletableFuture.supplyAsync(new Supplier<DealersResponse>() {
            @Override
            public DealersResponse get() {
                try {
                    final DealersResponse toDo = dealersGetDealer(datasetId, dealerId);
                    return toDo;
                } catch (ApiException e) {

                }
                return null;
            }
        });
        return future;
    }
}
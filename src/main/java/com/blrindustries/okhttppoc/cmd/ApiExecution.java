package com.blrindustries.okhttppoc.cmd;

import com.blrindustries.okhttppoc.exception.ApiException;
import com.blrindustries.okhttppoc.model.*;
import com.blrindustries.okhttppoc.api.DataSetApi;
import com.blrindustries.okhttppoc.api.DealersApi;
import com.blrindustries.okhttppoc.api.VehiclesApi;
import com.blrindustries.okhttppoc.model.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApiExecution {
    private static Logger LOGGER = LoggerFactory.getLogger(ApiExecution.class);

    @Autowired
    DataSetApi dataSetApi;

    @Autowired
    VehiclesApi vehiclesApi;

    @Autowired
    DealersApi dealersApi;

    /**
     * Performs asynchronous calls to the challenge API by utilizing Completable Futures.
     */
    public void executeRequestsAsynchronously() {

        String datasetId;
        List<Integer> vehicleIds;
        final long startTime = System.currentTimeMillis();
        try {
            DatasetIdResponse datasetResponse = dataSetApi.dataSetGetDataSetId();
            datasetId = datasetResponse.getDatasetId();

            VehicleIdsResponse vehicleIdsResponse = vehiclesApi.vehiclesGetIds(datasetId);
            vehicleIds = vehicleIdsResponse.getVehicleIds();

            // Asynchronously getting vehicle information
            List<CompletableFuture<VehicleResponse>> vehResFutures =
                    vehicleIds.stream()
                            .map(id -> vehiclesApi.getVehicleAsync(datasetId, id))
                            .collect(Collectors.toList());

            // Collecting vehicle responses
            List<VehicleResponse> vehicleResponses =
                    vehResFutures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            // Asynchronously getting dealer responses
            List<CompletableFuture<DealersResponse>> dealerResFutures =
                    vehicleResponses.stream()
                            .map(id -> dealersApi.getDealerAsync(datasetId, id.getDealerId()))
                            .collect(Collectors.toList());

            // Collecting dealer responses
            List<DealersResponse> dealersResponses =
                    dealerResFutures.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList());

            // Creating a mapping for the dealer id's and each vehicle
            Map<Integer, List<VehicleAnswer>> vehicleAnswerMap = new HashMap<>();
            vehicleResponses.forEach(vehicle -> {
                BiConsumer<VehicleAnswer, Integer> setVehicleId = VehicleAnswer::setVehicleId;
                BiConsumer<VehicleAnswer, Integer> setVehicleYear = VehicleAnswer::setYear;
                BiConsumer<VehicleAnswer, String> setVehicleMake = VehicleAnswer::setMake;
                BiConsumer<VehicleAnswer, String> setVehicleModel = VehicleAnswer::setModel;

                Supplier<VehicleAnswer> constructor = VehicleAnswer::new;
                VehicleAnswer vehicleAnswer = constructor.get();

                setVehicleId.accept(vehicleAnswer, vehicle.getVehicleId());
                setVehicleYear.accept(vehicleAnswer, vehicle.getYear());
                setVehicleMake.accept(vehicleAnswer, vehicle.getMake());
                setVehicleModel.accept(vehicleAnswer, vehicle.getModel());

                vehicleAnswerMap.computeIfAbsent(vehicle.getDealerId(), k -> new ArrayList<>()).add(vehicleAnswer);
            });

            // Simple map that maps dealer id to dealer name
            Map<Integer, String> dealerIdNameMap = new HashMap<>();
            dealersResponses.forEach(dealer -> {
                dealerIdNameMap.put(dealer.getDealerId(), dealer.getName());
            });

            // Generating the correct answer
            Answer answer = new Answer();
            vehicleAnswerMap.forEach((dealerId, listOfVehicles) -> {
                LOGGER.info("{}", dealerId);
                LOGGER.info("{}", listOfVehicles.toString());
                BiConsumer<DealerAnswer, Integer> setDealerId = DealerAnswer::setDealerId;
                BiConsumer<DealerAnswer, String> setDealerName = DealerAnswer::setName;
                BiConsumer<DealerAnswer, List<VehicleAnswer>> setVehicles = DealerAnswer::setVehicles;

                Supplier<DealerAnswer> constructor = DealerAnswer::new;
                DealerAnswer dealerAnswer = constructor.get();

                setDealerId.accept(dealerAnswer, dealerId);
                setDealerName.accept(dealerAnswer,  dealerIdNameMap.get(dealerId));
                setVehicles.accept(dealerAnswer, listOfVehicles);
                answer.addDealersItem(dealerAnswer);
            });

            LOGGER.info("Answer: {}", answer.toString());
            AnswerResponse response = dataSetApi.dataSetPostAnswer(datasetId, answer);
            LOGGER.info("response message: {}", response.getMessage());

            final long endTime = System.currentTimeMillis();
            LOGGER.info("Total execution time: {}", (endTime - startTime));

        } catch (ApiException e) {
            LOGGER.error("Program can not execute properly due to an APIException. See response: {}", e.getResponseBody());
        }
    }
}
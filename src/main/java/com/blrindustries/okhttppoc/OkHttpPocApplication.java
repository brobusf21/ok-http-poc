package com.blrindustries.okhttppoc;

import com.blrindustries.okhttppoc.cmd.ApiExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Profile("!test")
public class OkHttpPocApplication implements CommandLineRunner {

	@Autowired
    ApiExecution apiExecution;

	public static void main(String[] args) {
		SpringApplication.run(OkHttpPocApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		apiExecution.executeRequestsAsynchronously();
	}
}
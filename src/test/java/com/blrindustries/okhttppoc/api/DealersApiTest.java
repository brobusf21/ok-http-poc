package com.blrindustries.okhttppoc.api;

import static io.restassured.RestAssured.when;

import com.blrindustries.okhttppoc.exception.ApiException;
import com.blrindustries.okhttppoc.model.DatasetIdResponse;
import com.blrindustries.okhttppoc.model.DealersResponse;
import com.blrindustries.okhttppoc.model.VehicleIdsResponse;
import com.blrindustries.okhttppoc.model.VehicleResponse;
import java.util.List;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DealersApiTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DealersApiTest.class);
    private static final String BASEPATH = "";

    private String datasetId;
    private List<Integer> vehicleIds;
    private DataSetApi dataSetApi;
    private VehiclesApi vehiclesApi;
    private DealersApi dealersApi;

    @Before
    public void init() {
        try {
            dataSetApi = new DataSetApi();
            DatasetIdResponse response = dataSetApi.dataSetGetDataSetId();
            datasetId = response.getDatasetId();

            vehiclesApi = new VehiclesApi();
            VehicleIdsResponse vehiclesResponse = vehiclesApi.vehiclesGetIds(datasetId);
            vehicleIds = vehiclesResponse.getVehicleIds();

            dealersApi = new DealersApi();
        } catch (ApiException e) {
            LOGGER.error("API Exception caught initializing class DealersAPITest.");
        }
    }

    /**
     * Get information about a dealer
     *
     *
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void dealersGetDealerTest() {

        vehicleIds.forEach(vehicleInfo -> {
            try {
                VehicleResponse vehicleResponse = vehiclesApi.vehiclesGetVehicle(datasetId, vehicleInfo);
                DealersResponse dealerResponse = dealersApi.dealersGetDealer(datasetId, vehicleResponse.getDealerId());

                // Check if endpoint is up
                when().get(BASEPATH + "/api/" + datasetId + "/dealers/" + dealerResponse.getDealerId()).then().statusCode(HttpStatus.SC_OK);

            } catch (ApiException e) {
                LOGGER.error("API Exception caught during dealersGetDealerTest().");
            }
        });
    }
}
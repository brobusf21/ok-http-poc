package com.blrindustries.okhttppoc.api;

import static com.jcabi.matchers.RegexMatchers.matchesPattern;
import static io.restassured.RestAssured.when;

import com.blrindustries.okhttppoc.exception.ApiException;
import com.blrindustries.okhttppoc.model.DatasetIdResponse;
import com.blrindustries.okhttppoc.model.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataSetApiTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSetApi.class);
    private static final String BASEPATH = "";

    private String datasetId;
    private List<String> vehicleList;
    private DataSetApi dataSetApi;

    @Before
    public void setUp() throws ApiException {
        dataSetApi = new DataSetApi();
        DatasetIdResponse response = dataSetApi.dataSetGetDataSetId();
        datasetId = response.getDatasetId();
        LOGGER.info("datasetId: {}", datasetId);

        // Create list of vehicles
        vehicleList = new ArrayList();
        vehicleList.add("year: 2016\n" +
                "            make: Honda\n" +
                "            model: Accord\n");
        vehicleList.add("year: 2014\n" +
                "            make: Ford\n" +
                "            model: F150\n");
        vehicleList.add("year: 2004\n" +
                "            make: MINI\n" +
                "            model: Cooper\n");
        vehicleList.add("year: 2013\n" +
                "            make: Mitsubishi\n" +
                "            model: Gallant\n");
        vehicleList.add("year: 2009\n" +
                "            make: Ford\n" +
                "            model: F150\n");
        vehicleList.add("year: 2016\n" +
                "            make: Kia\n" +
                "            model: Soul\n");
        vehicleList.add("year: 2016\n" +
                "            make: Bentley\n" +
                "            model: Mulsanne\n");
        vehicleList.add("year: 2012\n" +
                "            make: Nissan\n" +
                "            model: Altima\n");
        vehicleList.add("year: 1979\n" +
                "            make: Cheverolet\n" +
                "            model: Corvette\n");
    }

    /**
     * Get correct answer for dataset (cheat)
     *
     *
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void dataSetGetCheatTest() throws ApiException {

        // Check endpoint is up
        when().get(BASEPATH + "/api/" + datasetId + "/cheat").then().statusCode(HttpStatus.SC_OK);

        Answer response = dataSetApi.dataSetGetCheat(datasetId);
        LOGGER.info("String: {}", response.toString());

        vehicleList.forEach(vehicleInfo -> {
            Assert.assertTrue(response.toString().contains(vehicleInfo));
        });
    }

    /**
     * Creates new dataset and returns its ID
     *
     *
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void dataSetGetDataSetIdTest() {

        // Test that the endpoint is up
        when().get(BASEPATH + "/api/datasetId").then().statusCode(HttpStatus.SC_OK);

        LOGGER.info("datasetId: {}", datasetId);

        // Assert that the datasetId is not null
        Assert.assertTrue(Objects.nonNull(datasetId));

        // Assert that the dataset length is 11
        Assert.assertTrue(datasetId.length() == 11);

        // Assert that the data is alphanumerical characters with a few special characters
        Assert.assertThat(datasetId, matchesPattern("[a-zA-Z0-9_]+"));
    }
}
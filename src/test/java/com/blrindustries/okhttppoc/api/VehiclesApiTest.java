package com.blrindustries.okhttppoc.api;

import static io.restassured.RestAssured.when;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.MatcherAssert.assertThat;

import com.blrindustries.okhttppoc.exception.ApiException;
import com.blrindustries.okhttppoc.model.DatasetIdResponse;
import com.blrindustries.okhttppoc.model.VehicleIdsResponse;
import com.blrindustries.okhttppoc.model.VehicleResponse;
import java.util.List;
import java.util.Objects;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VehiclesApiTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DealersApiTest.class);
    private static final String BASEPATH = "";
    private String datasetId;
    private List<Integer> vehicleIds;

    @Before
    public void init() {
        try {
            DataSetApi dataSetApi = new DataSetApi();
            DatasetIdResponse response = dataSetApi.dataSetGetDataSetId();
            datasetId = response.getDatasetId();

            VehiclesApi vehiclesApi = new VehiclesApi();
            VehicleIdsResponse vehiclesResponse = vehiclesApi.vehiclesGetIds(datasetId);
            vehicleIds = vehiclesResponse.getVehicleIds();

        } catch (ApiException e) {
            LOGGER.error("API Exception caught initializing VehicleApiTest");
        }
    }

    /**
     * Get a list of all vehicleids in dataset
     *
     *
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void vehiclesGetIdsTest() {

        // Check if endpoint is up
        when().get(BASEPATH + "/api/" + datasetId + "/vehicles").then().statusCode(HttpStatus.SC_OK);

        vehicleIds.forEach(vehicle -> {
            LOGGER.info("vehicle: {}", vehicle);
        });

        assertThat(vehicleIds, hasSize(9));
    }

    /**
     * Get information about a vehicle
     *
     *
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void vehiclesGetVehicleTest() {

        vehicleIds.forEach(vehicle -> {

            // Check if endpoint is up
            when().get(BASEPATH + "/api/" + datasetId + "/vehicles/" + vehicle).then().statusCode(HttpStatus.SC_OK);

            try {
                VehiclesApi vehiclesApi = new VehiclesApi();
                VehicleResponse vehicleResponse = vehiclesApi.vehiclesGetVehicle(datasetId, vehicle);

                Assert.assertTrue(Objects.nonNull(vehicleResponse.getDealerId()));
                Assert.assertTrue(Objects.nonNull(vehicleResponse.getMake()));
                Assert.assertTrue(Objects.nonNull(vehicleResponse.getModel()));
                Assert.assertTrue(Objects.nonNull(vehicleResponse.getYear()));
            } catch (ApiException e) {
                LOGGER.error("APIException caught. Issue with obtaining vehicle info in vehiclesGetVehicleTest()");
            }
        });
    }
}
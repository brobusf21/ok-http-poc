package com.blrindustries.okhttppoc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = ChallengeApplication.class)
public class ChallengeApplicationTests {

	@Test
	public void contextLoads() {
	}
}